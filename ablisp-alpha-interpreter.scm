;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                             ABLISP-ALPHA-INTERPRETER
;;
;; File: ablisp-alpha-interpreter.scm
;;
;; Description:
;; This file contains an exercise in developing a simple Lisp interpreter
;; in Scheme.
;;
;; Authors:
;;      Abhishek Chakravarti <abhishek@taranjali.org>
;;
;; Copyright:
;;      (c) 2019 Abhishek Chakravarti
;;      <abhishek@taranjali.org>
;;
;; License:
;;      Released under the GNU General Public License version 3 (GPLv3)
;;      <http://opensource.org/licenses/GPL-3.0>. See the accompanying LICENSE
;;      file for complete licensing details.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


#!/usr/bin/guile \ e - lisp-ab s
!#


;; The procedure that displays a line with an additional newline
(define (print-ab ln) (display ln) (newline))


;; REPL s-expression counter
(define repl-counter-ab 0)

;; Failure with message
(define (failure0-ab msg)
  (format #t "** AbLisp failure: ~s~%" msg)
  (throw 'failure0-ab msg))


;; Failure with message and value
(define (failure1-ab msg v0)
  (format #t "** AbLisp failure: ~s $1=~a;~%" msg v0)
  (throw 'failure1-ab msg v0))


;; Failure with message and 2 values
(define (failure2-ab msg v0 v1)
  (format #t "** AbLisp failure: ~s $1=~a; $2=~a~%" msg v0 v1)
  (throw 'failure2-ab msg v0 v1))


;; Failure with message and 3 values
(define (failure3-ab msg v0 v1 v2)
  (format #t "** AbLisp failure: ~s $1=~a; $2=~a; $3=~a~%" msg v0 v1 v2)
  (throw 'failure3-ab msg v0 v1 v2))


;; Failure with message and 4 values
(define (failure4-ab msg v0 v1 v2 v3)
  (format #t "** AbLisp failure: ~s $1=~a; $2=~a; $3=~a; $4=~a~%"
    msg v0 v1 v2 v3)
  (throw 'failure-ab msg v0 v1 v2))


;; Failure with message and 5 values
(define (failure5-ab msg v0 v1 v2 v3 v4)
  (format #t "** AbLisp failure: ~s $1=~a; $2=~a; $3=~a; $4=~a; $5=~a~%"
    msg v0 v1 v2 v3 v4)
  (throw 'failure4-ab msg v0 v1 v2 v3))


;; Trace message with no values
(define (trace0-ab msg)
  (format #t "** AbLisp trace: ~s~%" msg))


;; Trace message with 1 value
(define (trace1-ab msg v0)
  (format #t "** AbLisp trace: ~s $1=~a;~%" msg v0))


;; Trace message with 2 values
(define (trace2-ab msg v0 v1)
  (format #t "** AbLisp trace: ~s $1=~a; $2=~a~%" msg v0 v1))


;; Trace message with 3 values
(define (trace3-ab msg v0 v1 v2)
  (format #t "** AbLisp trace: ~s $1=~a; $2=~a; $3=~a~%" msg v0 v1 v2))


;; Trace message with 4 values
(define (trace4-ab msg v0 v1 v2 v3)
  (format #t "** AbLisp trace: ~s $1=~a; $2=~a; $3=~a; $4=~a~%"
    msg v0 v1 v2 v3))


;; Trace message with 5 values
(define (trace5-ab msg v0 v1 v2 v3 v4)
  (format #t "** AbLisp trace: ~s $1=~a; $2=~a; $3=~a; $4=~a; $5=~a~%"
    msg v0 v1 v2 v3 v4))


;; The (bind-env-ab!) procedure binds a variable to the hash table of an
;; environment vector.
(define (bind-env-ab! env var val)
  (hashq-set! (vector-ref env 0) var val))


;; The (get-binding-env-ab) procedure gets the value bound to a variable in the
;; hash table of an environment vector.
(define (get-binding-env-ab env var)
  (hashq-ref (vector-ref env 0) var))


;; The (bound-env-ab?) procedure checks if any value is bound to a variable
;; in the hash table of an environment vector.
(define (bound-env-ab? env var)
  (equal? (get-binding-env-ab env var)))


;; The (interaction-environment-ab) procedure returns the env-ab vector for now,
;; but I'm not sure if this is correct
(define (interaction-environment-ab)
  (let ((v (make-vector 4))
	(h (make-hash-table 100)))
    (begin
      (hashq-set! h 'ab-add (lambda (x y) (+ x y)))
      (hashq-set! h 'ab-mult (lambda (x y) (* x y)))
      (vector-set! v 0 h)
      v)))


(define (eval-bound-var-ab env sexpr)
  (apply
    (get-binding-env-ab env (car sexpr))
    (eval-ab (cdr sexpr) env)))


(define (eval-define-ab env sexpr)
  (if (equal? (cdr (cdr sexpr)) 'ab-define)
      (failure0-ab "ab-define used incorrectly")
      (bind-env-ab! env (car (cdr sexpr)) (cdr (cdr sexpr)))))


;; The (atom-ab?) procedure determines whether an S-expression is an atom.
(define (atom-ab? sexpr)
  (not (pair? sexpr)))

(define (eval-ab sexpr env)
  (cond ((atom-ab? sexpr)
	 (cond ((symbol? sexpr)
		(if (bound-env-ab? env sexpr)
		    (eval-bound-var-ab env sexpr)
		    (failure1-ab "symbol not bound to environment: " sexpr))
	       ((or (number? sexpr) (string? sexpr) (char? sexpr)
		    (boolean? sexpr) (vector? sexpr))
		sexpr))
	       (else (failure1-ab "cannot evaluate atom: " sexpr))))
	((equal? (car sexpr) 'ab-define) (eval-define-ab env sexpr))))

(define (eprogn-ab sexpr env)
  (if (pair? sexpr)
      (if (pair? (cdr sexpr))
	  (begin (eval-ab (car sexpr) env)
		 (eprogn-ab (cdr sexpr) env))
	  (eval-ab (car sexpr) env))
      '()))

;; The (eval-ab) procedure evaluates an S-expression, checking if there is
;; already a binding in the environment for a given symbol. I think that the
;; next step would be to add a condition for ab-define.
;; (define (eval-ab sexpr env)
;;  (cond ((bound-env-ab? env (car sexpr))
;;         (eval-bound-var-ab env sexpr))
;;	 ((equal? (car sexpr) 'ab-define)
;;          (eval-define-ab env sexpr))))


(define (lisp-ab args)
  (trace1-ab "command line arguments: " args)
  (display "Enter expression to evaluate: "))

;; TODO: the following code needs to be fixed, and has been commented out for
;; the time being
;;
;;(define (lisp2-ab)
;;  (display "Enter expression to evaluate: ")
;;    (let ((inp (read)))
;;      (if (not quit-ab? (inp))
;;        (begin
;;          (print-ab (eval-ab inp (interaction-environment-ab)))
;;          (+ 1 repl-counter-ab)
;;          (lisp2-ab)))))

